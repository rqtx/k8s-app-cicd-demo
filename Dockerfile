FROM golang:1.11-alpine as builder 

WORKDIR /usr/build 

# Download Go modules
COPY *.go ./
RUN go mod init demo/greetings
RUN go mod tidy
RUN go mod download

# Build
RUN CGO_ENABLED=0 GOOS=linux go build -o app

FROM scratch

WORKDIR /usr/bin
COPY --from=builder /usr/build/app .
EXPOSE 8080 

ENTRYPOINT ["/usr/bin/app"]
